const e2p = (s) => s.toString().replace(/\d/g, (d) => "۰۱۲۳۴۵۶۷۸۹"[d]);

const p2e = (s) =>
  s.toString().replace(/[۰-۹]/g, (d) => "۰۱۲۳۴۵۶۷۸۹".indexOf(d));

const sp = (number) => {
  const seperatedNumber = number
    .toString()
    .match(/(\d+?)(?=(\d{3})+(?!\d)|$)/g);
  const joinedNumber = seperatedNumber.join(",");
  return e2p(joinedNumber);
};

// -----------------------------------------------------------------

const addCommas = (num) => num?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
const removeNonNumeric = (num) => num?.toString().replace(/[^0-9]/g, '');
const convertPriceToNumber = (price) => {
  if (price !== 0 && typeof price !== 'number') {
    const numberArray = price.split(',');
    price = +numberArray.join('');
  }
  return price;
};




export { e2p, p2e, sp, addCommas, removeNonNumeric, convertPriceToNumber };