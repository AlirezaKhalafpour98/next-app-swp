const LoaderPage = () => {
    return (
        <div className="w-full h-screen bg-transparent flex flex-col gap-4 justify-center items-center text-center" >
            <span class="loader"></span>
        </div>
    );
}

export default LoaderPage;