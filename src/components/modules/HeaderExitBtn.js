'use client'

import { LogoutOutlined } from "@ant-design/icons";
import Cookies from "js-cookie";


const HeaderExitBtn = () => {

    const Logout = () => {
        Cookies.remove("accessTokenSwp")
        window.location.replace('/')
    }

    return (
        <div className="w-full border-t-2 pt-1" >
            <button className="text-red-600" onClick={() => Logout()}>
                <LogoutOutlined/> خروج 
            </button>
        </div>
    );
}

export default HeaderExitBtn;