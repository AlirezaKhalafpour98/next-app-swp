'use client'

import { ArrowUpOutlined } from "@ant-design/icons";

const ScrollTopBtn = () => {
    return (
        <div className="fixed bottom-24 right-2">
            <button onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })} className="rounded-full w-10 h-10 bg-[#675959] opacity-70 text-white " >
                <ArrowUpOutlined className="text-white" />
            </button>
        </div>
    );
}


export default ScrollTopBtn;