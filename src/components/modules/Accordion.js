import image1 from "../../../public/images/HomeImg.jpg"

const Accordion = () => {
    return (
        <div>
            <ul class="accordion">
                <li>
                    <img src='https://www.google.com/url?sa=i&url=https%3A%2F%2Fgithub.com%2FlAntraXxXl%2FFAQ_accordion&psig=AOvVaw39qWJIZNfMATr6o_IVwhIk&ust=1702823681016000&source=images&cd=vfe&opi=89978449&ved=0CBIQjRxqFwoTCOCzpdmWlIMDFQAAAAAdAAAAABAQ' />
                    <div class="content">
                    <span>
                        <h2>Carmen Rios</h2>
                        <p>Frontend</p>
                    </span>
                    </div>
                </li>
                <li>
                    <img src={image1} />
                    <div class="content">
                    <span>
                        <h2>Lisa Scott</h2>
                        <p>Backend</p>
                    </span>
                    </div>
                </li>
                <li>
                    <img src={image1} />
                    <div class="content">
                    <span>
                        <h2>Pavel Dvorak</h2>
                        <p>Backend</p>
                    </span>
                    </div>
                </li>
                <li>
                    <img src={image1} />
                    <div class="content">
                    <span>
                        <h2>Kelly Cox</h2>
                        <p>Designer</p>
                    </span>
                    </div>
                </li>
            </ul>
        </div>
    );
}

export default Accordion;