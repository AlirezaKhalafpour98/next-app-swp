'use client'

import React, { useEffect, useState } from 'react';
import { AutoComplete, Button, Divider, Drawer, Input, Radio, Slider, Space } from 'antd';
import { BarsOutlined, CloseCircleOutlined, FilterOutlined, SearchOutlined, TrophyOutlined } from '@ant-design/icons';
import axios from 'axios';
import { e2p } from '@/utils/replaceNumber';
import { addCommas } from '@persian-tools/persian-tools';
import Cookies from 'js-cookie';
import { useDispatch } from 'react-redux';
import { FilteredProductList } from '@/app/GlobalRedux/Features/filterData/FilterProductsDataSlice';


const marks = {
  0: e2p(0),
  1000000: `${1} میلیون`,
  3000000: `${3} میلیون`,
  5000000: {
    style: {
      color: '#f50',
    },
    label: <strong> {e2p(5)} میلیون </strong>,
  },
};




const FilterProducts = () => {


  const dispatch = useDispatch();

  const [open, setOpen] = useState(false);
  const [priceRange, setPriceRange] = useState([0,5000000]);
  // ----------------------
  const [filtersParameters, setFiltersParameters] = useState({
    searchQuery:"",
    // pageNumber: 1,
    // pageSize: 20,
    // orderBy: "",
    sellPriceMin: 0,
    sellPriceMax: 5000000,
    // productGroupId: 0,
    // themeId:0,
    // quantityStatus:0,
  })
  const formatter = (value) => `${e2p(addCommas(value))} تومان`;


  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };

  // ------------------------------------------

  const handlePriceRange = (e) => {
    setFiltersParameters({...filtersParameters, "sellPriceMin": e[0], "sellPriceMax": e[1]})
  }
  



  //Get Filtered product List --------------------
 
  const Headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json-patch+json'
  }


  const GetFilteredData = () => {
    // setLoading(true);
    console.log(filtersParameters, "filtered parametersssss")
    axios.post('https://www.soltaniwoodproducts.ir/api/Product/GetProductsBySearchParameters', filtersParameters, {headers:Headers})
      .then((response) => {
        // setLoading(false)
        console.log(response.data.result)
        dispatch(FilteredProductList(response.data.result))
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  // -Delete Filters------------------------

  const omitAllFilters = () => {
    setFiltersParameters({
      searchQuery:"",
      pageNumber: 0 ,
      pageSize: 0,
      orderBy: "",
      sellPriceMin:0,
      sellPriceMax: 5000000,
      productGroupId: 0,
      themeId:0,
      quantityStatus:0,
    });

    setOpen(false)
  }

  // Get GroupIds ----------------------------------

    // const GroupIdHeaders ={
    //   'accept': 'application/json',
    //   'Authorization': `Bearer ${Cookies.get("accessTokenSwp")}`
    // }
    // axios.get('https://www.soltaniwoodproducts.ir/api/ProductGroup/GetProductGroupList', {
    //   headers:GroupIdHeaders
    //   })
    //   .then((response) => {
    //     setProductGroups(response.data.result)
    //   })
    //   .catch((error) => {
    //     console.log(error, "Error");
    //   });


  return (
    <>
        <div className='w-full flex flex-row z-50 '>
            <button className='bg-mainBlack text-mainGray p-1 rounded-r-2xl' onClick={showDrawer}>
                فیلتر <FilterOutlined/>
            </button>
              <Input value={filtersParameters.searchQuery} onChange={(e)=> setFiltersParameters({...filtersParameters, searchQuery : `${e.target.value}`})} className='w-full border-mainBlack rounded-none ' size='middle' placeholder=" جستجو نام یا کد کالا " />
            <button onClick={() => GetFilteredData()} className='bg-mainBlack text-mainGray p-1 rounded-l-2xl w-20'>
              <SearchOutlined/>
            </button>
        </div>

      <Drawer
        className='rounded-t-3xl w-full'
        title=" فیلتر محصولات "
        placement="bottom"
        height={550}
        closable={false}
        onClose={onClose}
        open={open}
      >

      <div className='p-1 flex flex-col gap-10'>

        <div>
          <lable> بخش کالا </lable>
          <AutoComplete
              className='w-full '
              style={{borderColor:"black", color:"black"}}
              // options={productGroups}
              suffixIcon={<BarsOutlined/>}
              placeholder="بخش کالا"
              // filterOption={(inputValue, option) =>
              //   option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
              // }
              onChange={(e) => console.log(e)}
          />
        </div>

        
        <div>
          <lable> برند کالا </lable>
          <AutoComplete
              className='w-full'
              // options={options}
              suffixIcon={<BarsOutlined/>}
              placeholder="برند کالا"
              filterOption={(inputValue, option) =>
                option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
              }
          />
        </div>

        <div>
          <Radio.Group defaultValue="a" >
            <Radio.Button className='text-mainBlack bg-mainGray' value="a"> همه کالا ها </Radio.Button>
            <Radio.Button className='text-mainBlack bg-mainGray' value="b"> موجود </Radio.Button>
            <Radio.Button className='text-mainBlack bg-mainGray' value="c">ناموجود</Radio.Button>
          </Radio.Group>
        </div>

        <div>
          <label>  بازه قیمت  (تومان) </label>

            <Slider 
              tooltip={{formatter}}
              range 
              step={500000} 
              min={0} 
              max={5000000} 
              marks={marks} 
              defaultValue={[0, 5000000]} 
              onChange={(e) => handlePriceRange(e)}
            />

        </div>

        <div>
            <label> تم رنگی </label>
            <Radio.Group defaultValue="1" >
              <Radio.Button className='text-mainBlack bg-mainGray' value="1"> همه </Radio.Button>
              <Radio.Button className='text-mainBlack bg-mainGray' value="a"> روشن </Radio.Button>
              <Radio.Button className='text-mainBlack bg-mainGray' value="b"> تیره </Radio.Button>
              <Radio.Button className='text-mainBlack bg-mainGray' value="c"> خنثی </Radio.Button>
              <Radio.Button className='text-mainBlack bg-mainGray' value="c"> رنگی </Radio.Button>

            </Radio.Group>
        </div>

        <div className='w-full flex flex-row justify-center items-center py-3 border-t-2 gap-5'>
          <button onClick={() => GetFilteredData()} className='bg-mainBlack text-cyan-200 p-3 w-[40%] rounded-2xl flex justify-center items-center gap-1 '>
              <TrophyOutlined className='text-cyan-200'/>   اعمال فیلتر 
          </button>
          <button onClick={() => omitAllFilters()} className='bg-rose-200 p-3 rounded-2xl w-[40%] flex justify-center items-center gap-1 '>
              <CloseCircleOutlined className='tex-red-900'/> حذف فیلتر ها 
          </button>
        </div>

      </div>

      </Drawer>
    </>
  );
};
export default FilterProducts;