'use client'

import { addCart, increment, incrementBA } from "@/app/GlobalRedux/Features/counter/CounterSlice";
import { PlusCircleOutlined, ShoppingCartOutlined } from "@ant-design/icons";
import { message } from "antd";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

const AddToCardBtn = ({i}) => {
    

    const [messageApi, contextHolder] = message.useMessage();
    const error = () => {
      messageApi.open({
        type: 'error',
        duration: 3,
        content: " این کالا در سبد خریدتان موجود است. برای تغییر دادن تعداد وارد سبد خرید شوید ",
      });
    };

    const dispatch = useDispatch()

    const isInBasket = useSelector((state) => state.counter.items)

    function AddBtn() {
        dispatch(addCart(i))
        const isTrue = isInBasket.filter((id) => id.productId === i.productId)
        console.log(isTrue)
        if(isTrue.length > 0) {
            error()
        }
    }
    

    


    return (
        <div>
            {contextHolder}
            <button onClick={() => AddBtn()} className="rounded-2xl bg-mainBlack text-mainGray p-2 ">
                افزودن به سبد خرید <PlusCircleOutlined /> 
            </button>
        </div>
    );
}

export default AddToCardBtn;