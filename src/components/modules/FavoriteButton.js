'use client'

import { HeartOutlined } from "@ant-design/icons";

const FavoriteButton = () => {
    return (
        <div>
            <button>
                <HeartOutlined/>
            </button>
        </div>
    );
}

export default FavoriteButton;