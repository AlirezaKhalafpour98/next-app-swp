import Image from 'next/image'
import homepng from '../../../public/images/HomeImg.jpg'
 
export default function BackgroundMainImage() {
  return (
    <Image
      alt="main image soltani wood products"
      src={homepng}
      placeholder="blur"
      quality={20}
      fill
      sizes="100vw"
      style={{
        objectFit: 'cover',
      }}
    />
  )
}