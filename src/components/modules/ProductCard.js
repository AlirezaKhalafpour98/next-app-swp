'use client'

import { CreditCardOutlined, PayCircleOutlined } from "@ant-design/icons";
import Link from "next/link";
// import { cookies } from "next/headers";
import axios from "axios";
import dynamic from "next/dynamic";
import { e2p } from "@/utils/replaceNumber";
import { addCommas } from "@persian-tools/persian-tools";
import AuthImg from "../../../public/images/Authentication-cuate.svg"
import Image from "next/image";
import { useSelector } from "react-redux";

const AddToCardBtn = dynamic(() => import("./AddToCardBtn"),{
    ssr: false,
    loading: () => <p> i am loading ... </p>
  })




async function ProductCard() {

    const filterData = useSelector((state) => state.filteredData.productItems)

    
        //     const res = await axios.get( "https://www.soltaniwoodproducts.ir/api/Product/GetProductList", { headers: { 
        //         'accept': 'application/json',
        //         'Authorization': `Bearer ${cookies().get('accessTokenSwp')?.value}`
        //     },
        //         cache: "no-store"
        //     },).catch((err) => {
        //         console.log(err)
        //         // redirect("/")
        //     })
              
        // const productList =  res?.data?.result;

        const isFiltered = true

    return (
        <>

        <div className="flex flex-col gap-4 items-center my-20" >

            {/* {productList?.length > 0  ? null : (

                <>
                    <Image src={AuthImg} width={300} height={300} />
                    <h2>
                        ابتدا وارد شوید
                    </h2>
                    <Link href='/signin' className="p-2 w-40 bg-yellow-500 rounded-xl" > ورود </Link>
                </>
            )
            } */}
            

            {

                isFiltered === false ? 

                // productList?.map((i) => (

                //     <div key={i.productId} className="flex flex-col justify-center items-center gap-2 w-[86%] bg-[#bfad9d30] rounded-xl mb-6 p-2" >

                //         <div className="max-w-min p-2 text-center hover:shadow-2xl " >
                //             <Image
                //                 className="rounded-xl"
                //                 width={260}
                //                 src= {i.images[0] ? i.images[0] : ""}
                //                 alt={i.productName}
                //             />
                //         </div>
                //         <Link href={`/products/${i.productId}`} className="flex flex-col justify-start items-start w-[75vw] gap-2 p-4">
                //             <h2 className="font-semibold">  {i.productName} </h2>
                //             {/* <span>  ابعاد {i.size} </span> */}
                //             <span>  کد کالا {e2p(i.code)} </span> 
                //             <span className="gap-2" > <CreditCardOutlined/> {e2p(addCommas(i.salePrice))}  </span>
                //             {/* <Badge count={"موجود"} color='cyan' className="w-20 z-0"/> */}
                //         </Link>

                //         <div className="p-3">
                //             <AddToCardBtn i={i}/>
                //         </div>

                //     </div>
                // ))
                (<p>cfdsfs</p>)

                :

                filterData?.map((i) => (

                    <div key={i.productId} className="flex flex-col justify-center items-center gap-2 w-[86%] bg-[#bfad9d30] rounded-xl mb-6 p-2" >

                        <div className="max-w-min p-2 text-center hover:shadow-2xl " >
                            <Image
                                className="rounded-xl"
                                width={260}
                                // src= {i.images[0] ? i.images[0] : ""}
                                alt={i.productName}
                            />
                        </div>
                        <Link href={`/products/${i.productId}`} className="flex flex-col justify-start items-start w-[75vw] gap-2 p-4">
                            <h2 className="font-semibold">  {i.productName} </h2>
                            {/* <span>  ابعاد {i.size} </span> */}
                            <span>  کد کالا {e2p(i.code)} </span> 
                            <span className="gap-2" > <CreditCardOutlined/> {e2p(addCommas(i.salePrice))}  </span>
                            {/* <Badge count={"موجود"} color='cyan' className="w-20 z-0"/> */}
                        </Link>

                        <div className="p-3">
                            <AddToCardBtn i={i}/>
                        </div>

                    </div>
                ))
                
            }

        </div>
            
        </>
    );
}

export default ProductCard;