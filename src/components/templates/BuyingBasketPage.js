'use client'

import { useEffect, useMemo, useState } from "react";
import { addCart, decrement, increment } from "@/app/GlobalRedux/Features/counter/CounterSlice";
import { addCommas, e2p } from "@/utils/replaceNumber";
import { Avatar, Divider, Empty, Input, InputNumber, Progress, Steps } from "antd";
import { useDispatch, useSelector } from "react-redux";
import EmptyPic from "../../../public/images/Empty.svg"
import Image from "next/image";
import { AlignRightOutlined, LoadingOutlined, ShoppingFilled, SmileOutlined, SolutionOutlined, UserOutlined } from "@ant-design/icons";
import Link from "next/link";
import { numberToWords } from "@persian-tools/persian-tools";


const BuyingBasketPage = () => {


  const cart = useSelector((state) => state.counter.items)
  let sumProduct = useSelector((state) => state.counter.sumProduct)
  const WholePrice = useSelector((state) => state.counter.sumPrice)
  const dispatch = useDispatch()

  // const [prices , setPrices] = useState(0)

  // const [eachItemPrice, setEachItemPrice] = useState([])

  // const CalcSumPrice = () => {
  //   cart?.map((i) => {
  //     const tempPrice = i.salePrice
  //     const tempQty = i.cartQty
  //     const wholePrice = tempPrice * tempQty
  //     setPrices( (prev) => prev +  wholePrice)
  //   })

  // }
  // useEffect(() =>{
  //   CalcSumPrice();
  // },[cart])

  // console.log(prices, "........")

  return(

    <>

      <div className="w-full flex flex-col gap-1 justify-center items-center mt-8 " >

      <div className="w-full flex flex-row justify-normal items-center" >
          <div className="w-1/5 text-center " >
              <Avatar className="bg-yellow-500" size={"large"} icon={<ShoppingFilled />} />
          </div>
          <div className="w-4/5 flex flex-col items-start " >
              <p className="text-sm" > مرحله {e2p(1)} از {e2p(5)} </p>
              <p className="text-lg text-yellow-600 font-semibold " > اقلام سبد خرید  </p>
          </div>
      </div>

      <div className="w-[90%]" >

          <Progress percent={20} status="active" showInfo={false} />

      </div>
      </div>
      <div className="flex flex-col justify-between  h-full ">

        {
          cart?.length > 0 
          ?
          cart?.map((i) => (
            <div className="w-[80%] h-64 flex flex-col justify-center items-center mx-auto gap-2">

            <div className="w-full h-3/4 flex flex-row justify-center items-center" >
    
              <div className="w-1/3" id="image" ></div>
              <div className="w-2/3 flex flex-col gap-1">
                <h3>{ i.productName }</h3>
                <p className="text-sm" >  قیمت: {e2p(addCommas(i.salePrice))} </p>
              </div>
    
            </div>
    
            <div className="w-full h-1/4 p-1 flex row justify-between items-center gap-2">
    
            <div className="flex flex-row-reverse justify-center items-center w-1/2" >
            <button onClick={() => dispatch(decrement(i.productId)) } className="w-1/3 border-2 bg-mainBlack rounded-lg rounded-r-none h-9 text-mainGray " > - </button>
              <InputNumber  max={5} value={i.cartQty} className="w-1/3 h-[35px] rounded-none "/>
              <button disabled={i.cartQty >= 5} onClick={() => dispatch(increment(i.productId)) } className="w-1/3 border-2 disabled:bg-red-300 bg-mainBlack rounded-lg rounded-l-none h-9 text-mainGray " > + </button>
    
            </div>
    
            <p className="text-sm"> قیمت کل : {e2p(addCommas(i.salePrice * i.cartQty))}</p>
    
            </div>
    
          <Divider/>
          </div>
          
          ))
          :
          <div className="flex flex-col gap-2 justify-start items-center w-full h-full" >
            <Image src={EmptyPic} width={300} height={300} />
            <p className="text-lg" > سبد خرید خالی است... </p>
          </div>
        }

        {
          cart.length > 0 &&
          <div className="w-full flex flex-row gap-1 justify-around items-end p-4 h-28 rounded-t-2xl border-2 border-b-0 bg-gray-200 border-mainBlack  " >

            <div className="flex flex-col justify-center items-start gap-2 text-sm " >
              <div> <AlignRightOutlined /> تعداد کل: {e2p(sumProduct)} </div>
              <div> <ShoppingFilled/> جمع کل سبد: {e2p(addCommas(WholePrice))} </div>
              <div> <ShoppingFilled/> جمع کل سبد: {numberToWords(WholePrice)} ریال </div>

            </div>

            <Link href="/buying-basket/buying-basket-address" className="bg-yellow-500 p-1 w-24 rounded-xl text-mainBlack" > ادامه خرید </Link>

          </div>
        }


      </div>

    </>
  )

  };
export default BuyingBasketPage;