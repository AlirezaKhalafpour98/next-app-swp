'use client'

import { ArrowLeftOutlined, EyeInvisibleOutlined, EyeTwoTone, KeyOutlined, LoadingOutlined, LockOutlined, LoginOutlined, MobileOutlined, UserAddOutlined, UserOutlined } from "@ant-design/icons";
import Image from 'next/image'
import Signup from "../../../public/images/SignupR.svg"
import { Button, Checkbox, Divider, Input, Space, Statistic, message } from "antd";
import { useState } from "react";
import axios from "axios";
import { e2p, p2e } from "@/utils/replaceNumber";
import Cookies from "js-cookie";
import Link from "next/link";


const SignUpPage = () => {

    const [loading, setLoading] = useState(false);
    const [name, setName] = useState("");
    const [lastName, setLastName] = useState("");
    const [number, setNumber] = useState("");
    const [username, setUsername] = useState("");
    const [countOn, setCountOn] = useState(false);


    const { Countdown } = Statistic;

    // // Massage Toast------------------------------------------

    // const [messageApi, contextHolder] = message.useMessage();
    // const success = () => {
    //   messageApi.open({
    //     type: 'success',
    //     content: ' خوش آمدید ',
    //   });
    // };
    // const error = (err) => {
    //   messageApi.open({
    //     type: 'error',
    //     content: err,
    //   });
    // };

    // -------------------------------------------------------

    async function ConnectApi(){
        setLoading(true);
        setCountOn(true);
        await axios.post('https://www.soltaniwoodproducts.ir/api/RegisterUser/AddUserInformSmp', {
            "username": username,
            "firstName":name,
            "lastName":lastName,
            "phoneNumber": p2e(number)
          })
          .then((response) => {
            setLoading(false)
            console.log(response)
            // if(response.data.errors && response.data.status !== 0){
            //     error(response.data.errors)
            // }else{
            //     success()
            //     const token = response.data.result.accessToken
            //     Cookies.set('accessToken', token, {expires:1 , secure: true });
            //     setTimeout(() => {
            //         window.location.replace("/");
            //     }, 1500);
            // }
          })
          .catch(function (error) {
            console.log(error);
          });
    }


    function RegisterHandler() {
        ConnectApi()
    }
    function FinishCountDown() {
        setCountOn(false);
    }

    async function CodeValidation(smsCode){

        if (smsCode.length >=4 ){
            await axios.get(`https://www.soltaniwoodproducts.ir/api/RegisterUser/ValidSmsCode?smsCode=${smsCode}`, {headers:'accept: application/json'})
            .then((response) => {
              setLoading(false)
              console.log(response)
              // if(response.data.errors && response.data.status !== 0){
              //     error(response.data.errors)
              // }else{
              //     success()
              //     const token = response.data.result.accessToken
              //     Cookies.set('accessToken', token, {expires:1 , secure: true });
              //     setTimeout(() => {
              //         window.location.replace("/");
              //     }, 1500);
              // }
            })
            .catch(function (error) {
              console.log(error);
            });
        }
    }


    return (
        <>
            <div className="w-full h-screen">
                <div className="w-full flex flex-col p-5 gap-2 ">
                    <div className="w-full flex justify-end items-center">
                        <Link href="/" className="bg-mainBlack text-white rounded-full p-1" >
                            <ArrowLeftOutlined/>
                        </Link>
                    </div>
                    <div className="w-full h-full flex justify-center items-center mt-3 mb-3 ">
                        <Image src={Signup} width={220} height={220} />
                    </div>

                    <div className="w-full flex flex-col justify-center items-center gap-8" >
                        <Input 
                            className="w-[90%] border-1 border-mainBlack" 
                            onChange={(e) => setUsername(e.target.value)}
                            value={username}
                            size="large" 
                            placeholder="نام کاربری" 
                            prefix={<UserOutlined />} 
                        />
                        <Input
                            className="w-[90%] border-1 border-mainBlack"
                            onChange={(e)=> setName(e.target.value)}
                            value={e2p(name)}
                            placeholder=" نام "
                            size="large"
                            prefix={<UserOutlined />}
                        />
                        <Input
                            className="w-[90%] border-1 border-mainBlack"
                            onChange={(e)=> setLastName(e.target.value)}
                            value={e2p(lastName)}
                            placeholder=" نام خانوادگی "
                            size="large"
                            prefix={<UserOutlined />}
                        />
                        <Input
                            className="w-[90%] border-1 border-mainBlack"
                            onChange={(e)=> setNumber(e.target.value)}
                            value={e2p(number)}
                            placeholder=" شماره همراه (سیم کارت به نام خود شخص) "
                            size="large"
                            prefix={<MobileOutlined />}
                        />
                            <Space.Compact className="w-full text-center" >
                                {
                                    countOn 
                                    ?
                                    <div className="bg-mainGray border-black border rounded-l-lg p-1 " >
                                        <Countdown  style={{fontSize: '10px',  }} className="text-white" value={Date.now() + 60 * 1000 * 2} onFinish={() => FinishCountDown()} />
                                    </div>
                                    :
                                    <Button onClick={() => RegisterHandler()} size="large" className="bg-mainBlack text-white"  > دریافت کد </Button>
                                }
                            </Space.Compact>

                        <Input onChange={(e) => CodeValidation(e.target.value)} size="large" placeholder=" کد ارسالی را وارد کنید " />

                        {/* <button 
                            onClick={() => CodeValidation()}
                            className="w-[50%] rounded-2xl p-2 gap-3 flex flex-row justify-center items-center text-mainGray bg-mainBlack font-semibold"> 
                            <UserAddOutlined className="font-bold" 
                        /> 
                            {loading ? <LoadingOutlined style={{ fontSize: 20 }} spin /> : " ثبت نام "} 
                        </button> */}
                        
                    </div>
                    <Divider/>
                    <Link href="/signin" className="text-center text-sky-700 font-medium mb-16 ">  قبلا ثبت نام کردی؟ ورود </Link>

                </div>   
            </div>
        </>
    );
}

export default SignUpPage;