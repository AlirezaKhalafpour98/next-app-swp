'use client'

import { EditOutlined, MailOutlined } from "@ant-design/icons";
import { Avatar, Badge, Input } from "antd";
import TextArea from "antd/es/input/TextArea";

const ProfileMainPage = () => {
    return (
        <div className="flex flex-col gap-14 w-full p-6">
        <div className="flex md:flex-row flex-col justify-around items-center gap-6" >


            <Avatar alt="Aravis Howard" size="lg" className="md:w-64 w-40 md:h-64 h-40 border-2 border-paszamine2" />


            <div className="gap-3 flex flex-col justify-center items-center">
                <h2 className="text-2xl font-bold"> آرمین زارعی </h2>
                <div> <MailOutlined className="text-khas"/> www.email@gmail.com </div>
            </div>

        </div>

        <div className="flex md:flex-row flex-col justify-around items-center gap-4" >

            <div className="w-full flex flex-col gap-1 ">
                <span className="border-b" > نام </span>
                <span className="md:w-[90%] w-full "> آرمین زارعی </span>
                
            </div>

            <div className="w-full flex flex-col gap-1 ">
                <span className="border-b" > نام </span>
                <span className="md:w-[90%] w-full "> آرمین زارعی </span>
                
            </div>

        </div>

        <div className="flex md:flex-row flex-col justify-around items-center gap-4" >

            <div className="w-full flex flex-col gap-1 ">
                <span className="border-b" > نام </span>
                <span className="md:w-[90%] w-full "> آرمین زارعی </span>
                
            </div>

            <div className="w-full flex flex-col gap-1 ">
                <span className="border-b" > نام </span>
                <span className="md:w-[90%] w-full "> آرمین زارعی </span>
                
            </div>

        </div>

        <div className="flex md:flex-row flex-col justify-center items-center" >

            <div className="w-full flex flex-col  " >
                <span className="border-b" > آدرس و کد پستی </span>
                <span className="md:w-[90%] w-full"> Hnvs </span>
                
            </div>

        </div>

        
    </div>
    );
}

export default ProfileMainPage;