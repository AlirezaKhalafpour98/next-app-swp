'use client'

import { EditOutlined, MailOutlined } from "@ant-design/icons";
import { Avatar, Badge, Input } from "antd";
import TextArea from "antd/es/input/TextArea";

const ProfileEditPage = () => {
    return (
        <div className="flex flex-col gap-14 w-full p-6">
        <div className="flex md:flex-row flex-col justify-around items-center gap-6" >


            <button type="button
            " onClick={() => alert("ویرایش عکس")}>
                <Badge.Ribbon text="ویرایش" color="cyan">
                    <Avatar alt="Aravis Howard" size="lg" className="md:w-64 w-40 md:h-64 h-40 border-2 border-paszamine2" />
                </Badge.Ribbon>
            </button>

            <div className="gap-3 flex flex-col justify-center items-center">
                <h2 className="text-2xl font-bold"> آرمین زارعی </h2>
                <p> <MailOutlined className="text-khas"/> www.email@gmail.com </p>
            </div>

        </div>

        <div className="flex md:flex-row flex-col justify-around items-center gap-4" >

            <div className="w-full flex flex-col ">
                <span> نام </span>
                <Input className="md:w-[90%] w-full shadow-lg " size="lg" placeholder=" نام " />
                
            </div>

            <div className="w-full flex flex-col " >
                <span> نام خانوادگی </span>
                <Input className="md:w-[90%] w-full shadow-lg " size="lg" placeholder="  نام خانوادگی" />
                
            </div>

        </div>

        <div className="flex md:flex-row flex-col justify-around items-center gap-4" >

            <div className="w-full flex flex-col  " >
                <span> شماره همراه </span>
                <Input className="md:w-[90%] w-full shadow-lg " size="lg" placeholder="   شماره همراه  " />
                
            </div>

            <div className="w-full flex flex-col  ">
                <span> ایمیل </span>
                <Input className="md:w-[90%] w-full shadow-lg " size="lg" placeholder=" ایمیل " />
                
            </div>

        </div>

        <div className="flex md:flex-row flex-col justify-center items-center" >

            <div className="w-full flex flex-col  " >
                <span> آدرس و کد پستی </span>
                <TextArea className="md:w-[90%] w-full text-center shadow-lg " allowClear placeholder="   آدرس و کد پستی  " />
                
            </div>

        </div>

        <div className="flex flex-row justify-center items-center w-full" >

            <button className="bg-mainBlack p-3 rounded-full shadow-2xl md:w-52 w-32 text-white hover:font-bold  transition-all duration-150" >
                ذخیره تغییرات
            </button>

        </div>

        
    </div>
    );
}

export default ProfileEditPage;