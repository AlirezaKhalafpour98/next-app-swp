import { ArrowLeftOutlined, PlusCircleOutlined } from "@ant-design/icons";
import { Carousel, Checkbox, Divider, InputNumber, Rate } from "antd";
import Link from "next/link";
import FavoriteButton from "../modules/FavoriteButton";
import AddToCardBtn from "../modules/AddToCardBtn";
import axios from "axios";
import { cookies } from "next/headers";
import image1 from "../../../public/images/HomeImg.jpg"
import Image from "next/image";
import { e2p } from "@/utils/replaceNumber";
import AuthImg from "../../../public/images/Authentication-cuate.svg"

async function SamplePage({params:{ productId }}) {

    const mycookie = cookies().get("accessTokenSwp").value
    
    const res = await axios.get( `https://www.soltaniwoodproducts.ir/api/Product/GetProduct?productId=${productId}`, { headers: { 
        'accept': 'application/json',
        'Authorization': `Bearer ${mycookie}`
    },
        cache: "no-store"
    }).catch((err) => {
        console.log(err)
    })


    const data = await res?.data.result;

    const productImage = data?.images[0]?.image || ""


    return (
        <div className="w-full flex flex-col gap-2" >


            {data ? null : (

            <h2>
            ایتدا وارد شوید
            <Image src={AuthImg} width={300} height={300} />
            </h2>

            )
            }




            <div className="w-full relative">
                <Image
                    className="w-full h-full relative"
                    src={productImage}
                    width={230}
                    height={230}
                />
                <Link className="bg-mainGray absolute rounded-full top-3 left-4 p-1 " href="/products">
                    <ArrowLeftOutlined className="text-mainBlack"/>
                </Link>
                    
            </div>
            <div className="w-full flex flex-col justify-start items-start p-2 gap-2" >
                
                <div className="flex flex-row justify-between items-center w-full">
                    <h1 className="text-xl font-semibold"> {data?.title} </h1>
                    <FavoriteButton  />
                </div>
                <h2> {data?.code} کد: </h2>
                <h3 className="text-sm"> ابعاد 48 * 366 </h3>
                <h2>  {e2p(data?.salePrice)} </h2>
                <Rate defaultValue={4} />
                <Divider/>
                <div className="flex flex-col justify-center items-start" >

                    <h3 className="text-lg"> توضیحات </h3>
                    <p className="text-sm"> {data?.description} </p>

                </div>
                <Divider/>
                <div className="flex flex-row w-full justify-around">
                    <h3> تعداد: </h3>
                    <InputNumber
                        addonAfter={<PlusCircleOutlined />}
                        keyboard={true}
                    />
                </div>
                <Divider/>
                <div className="w-full text-center" >

                    <AddToCardBtn  />

                </div>

                <div className="flex flex-col w-full justify-center items-center gap-3 mt-16" >

                    {/* <div className="h-full w-full">
                        <ProductSwiper data={data} />
                    </div> */}


                </div>



            </div>
        </div>
    );
}

export default SamplePage;