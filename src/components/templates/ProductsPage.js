import { Divider } from "antd";
import ProductCard from "../modules/ProductCard";
import ScrollTopBtn from "../modules/ScrollTopBtn";
import dynamic from "next/dynamic";

const FilterProducts = dynamic(() => import("../modules/FilterProducts"),{
    ssr: false,
    loading: () => <p> i am loading ... </p>
  })

const ProductsPage = () => {
    return (
        <div className=" flex flex-col p-2 justify-start relative items-center gap-4 ]">
            <div className="w-full fixed top-[44px]">
                <FilterProducts />
            </div>
            <Divider/>
            <ScrollTopBtn className="absolute  z-50" />
            <h2> کالای مورد نظر را بیابید و به سبد خود اضافه کنید </h2>
            <ProductCard />
        </div>
    );
}

export default ProductsPage;