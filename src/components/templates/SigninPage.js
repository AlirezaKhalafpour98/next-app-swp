'use client'

import { ArrowLeftOutlined, EyeInvisibleOutlined, EyeTwoTone, KeyOutlined, LoadingOutlined, LoginOutlined, UserOutlined } from "@ant-design/icons";
import Image from 'next/image'
import logoSI from "../../../public/logo/SWP_new1.png"
import { Button, Checkbox, Divider, Input, message } from "antd";
import { useState } from "react";
import Link from "next/link";
import { e2p, p2e } from "@/utils/replaceNumber";
import axios from "axios";
import Cookies from "js-cookie";


const SigninPage = () => {

    const [loading, setLoading] = useState(false);
    const [pass, setPass] = useState("");
    const [username, setUsername] = useState("");


    // Massage Toast------------------------------------------

    const [messageApi, contextHolder] = message.useMessage();
    const success = () => {
      messageApi.open({
        type: 'success',
        content: ' خوش آمدید ',
      });
    };
    const error = (err) => {
      messageApi.open({
        type: 'error',
        content: err,
      });
    };

    // -------------------------------------------------------

    async function ConnectApi(){
        setLoading(true);
        await axios.post('https://www.soltaniwoodproducts.ir/api/Account/Login', {
            "username": username,
            "password": p2e(pass)
          })
          .then((response) => {
            setLoading(false)
            if(response.data.errors && response.data.status !== 0){
                error(response.data.errors)
            }else{
                success()
                const token = response.data.result.accessToken
                Cookies.set("accessTokenSwp" , token , {expires: new Date(Date.now + 50000 )})
                setTimeout(() => {
                    window.location.replace("/");
                }, 1500);
            }
          })
          .catch(function (error) {
            console.log(error);
          });
    }


    function SignInHandler() {
        ConnectApi()
    }

    const onChange = (e) => {
        console.log(`checked = ${e.target.checked}`);
      };

    return (
        <>
            <div className="w-full h-screen">
                {contextHolder}
                <div className="w-full flex flex-col p-5 gap-2 ">
                    <div className="w-full flex justify-end items-center">
                        <Link href="/" className="bg-mainBlack text-white rounded-full p-1" >
                            <ArrowLeftOutlined/>
                        </Link>
                    </div>
                    <div className="w-full h-28 flex justify-center items-center mt-4 mb-8 ">
                        <Image src={logoSI} priority className="w-32 h-16" />
                    </div>

                    <div className="w-full flex flex-col justify-center items-center gap-8" >
                        <Input 
                            className="w-[90%] border-1 border-mainBlack" 
                            onChange={(e) => setUsername(e.target.value)}
                            value={username}
                            size="large" 
                            placeholder="نام کاربری" 
                            prefix={<UserOutlined />} 
                        />
                        <Input.Password
                            className="w-[90%] border-1 border-mainBlack"
                            onChange={(e)=> setPass(e.target.value)}
                            value={e2p(pass)}
                            placeholder="رمز عبور"
                            size="large"
                            prefix={<KeyOutlined/>}
                            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                        />
                        <Checkbox onChange={onChange} className="font-semibold">مرا بخاطر بسپار</Checkbox>
                        <button 
                            onClick={() => SignInHandler()} 
                            className="w-[50%] rounded-2xl p-3 gap-3 flex flex-row justify-center items-center text-mainGray bg-mainBlack font-bold"> 
                            <LoginOutlined className="font-bold" 
                        /> 
                            {loading ? <LoadingOutlined style={{ fontSize: 24 }} spin /> : "ورود "} 
                        </button>
                        
                    </div>
                    <Divider/>
                    <Link href="/signup" className="text-center text-sky-700 font-medium"> حساب کاربری نداری؟ثبت نام </Link>

                </div>   
            </div>
        </>
    );
}

export default SigninPage;