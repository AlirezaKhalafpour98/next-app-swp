'use client'

import { BellOutlined, EditOutlined, HeartOutlined, SettingOutlined, UserOutlined } from "@ant-design/icons";
import { Badge, List } from "antd";
import Link from "next/link";

const ProfileSidebar = ({children}) => {
    return (
        <div className="w-full flex flex-row justify-center gap-3">

            <div className="w-1/6" >
                <List className="border-l-2 border-gray-300 h-full font-extrabold">

                    <h1 className="w-full text-center md:block hidden py-12 md:text-2xl text-lg" > پروفایل </h1>

                    <List.Item className="mb-3">
                    <Link href="/profile" className='  w-full shadow-none border-l-4 hover:border-mainBlack hover:bg-gray-300 text-gray-500 hover:text-mainBlack transition-all duration-200 flex justify-start md:mr-4 mr-2 cursor-pointer p-2  rounded-md gap-2' >
                        <UserOutlined className="text-xl" />
                        <p className="md:block hidden" > اطلاعات کاربر </p>
                    </Link>
                    </List.Item>
                    <List.Item className="mb-3">
                    <Link href="/profile" className='  w-full shadow-none border-l-4 hover:border-mainBlack hover:bg-gray-300 text-gray-500 hover:text-mainBlack transition-all duration-200 flex justify-start md:mr-4 mr-2 cursor-pointer p-2  rounded-md gap-2' >
                        <EditOutlined className="text-xl" />
                        <p className="md:block hidden" > ویرایش اطلاعات </p>
                    </Link>
                    </List.Item>
                    <List.Item className="mb-3" >
                    <Link href="/profile" className=' w-full shadow-none border-l-4 hover:border-mainBlack hover:bg-gray-300 text-gray-500 hover:text-mainBlack transition-all duration-200 flex justify-start md:mr-4 mr-2 cursor-pointer p-2 rounded-md gap-2' >
                        <HeartOutlined className="text-xl" />
                        <p className="md:block hidden" > لیست علاقه مندی ها </p>
                    </Link>
                    </List.Item>
                    <List.Item className="mb-3" >
                    <Link href="/profile" className='  w-full shadow-none border-l-4 hover:border-mainBlack hover:bg-gray-300 text-gray-500 hover:text-mainBlack transition-all duration-200 flex justify-start md:mr-4 mr-2 cursor-pointer p-2 rounded-md gap-2' >
                        <SettingOutlined className="text-xl" />
                        <p className="md:block hidden" > تنظیمات </p>
                    </Link>
                    </List.Item>
                    <List.Item className="mb-3" >
                    <Link href="/profile" className='  w-full shadow-none border-l-4 hover:border-mainBlack hover:bg-gray-300 text-gray-500 hover:text-mainBlack transition-all duration-200 flex justify-start md:mr-4 mr-2 cursor-pointer p-2 rounded-md gap-2' >
                        <Badge >
                            <BellOutlined className="text-xl" />
                        </Badge>
                        <p className="md:block hidden" > اعلان ها </p>
                    </Link>
                    </List.Item>


                </List>
            </div>

            
            <div className="w-5/6" >
                {children}
            </div>
        </div>
    );
}

export default ProfileSidebar;