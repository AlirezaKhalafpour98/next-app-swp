'use client'

import { Avatar, Button, Flex, FloatButton, Segmented } from "antd";
import { BarsOutlined, HomeFilled, HomeOutlined, PayCircleFilled, PhoneOutlined, QuestionCircleOutlined, SyncOutlined, UserOutlined } from '@ant-design/icons';
import Link from "next/link";

const MobileBottomBar = () => {
    return (
        <div className="fixed bottom-0 w-full mb-0" >

            <Segmented
                className="w-full rounded-t-2xl mb-0 bottom-0"
                options={[
                    {
                    label: (
                        <Link
                        style={{
                            width: "100%",
                        }}
                        href='./products'
                        >
                        <Avatar
                            style={{
                            backgroundColor: 'black',
                            }}
                        >
                            <BarsOutlined/>
                        </Avatar>
                        <div className="text-xs" >محصولات</div>
                        </Link>
                    ),
                    value: 'user1',
                    },
                    {
                    label: (
                        <Link
                        href='./'
                        style={{
                            width: "100%",
                        }}
                        >
                        <Avatar
                            style={{
                            backgroundColor: 'black',
                            }}
                        >
                            <HomeFilled/>
                        </Avatar>
                        <div className="text-xs"> خانه </div>
                        </Link>
                    ),
                    value: 'user2',
                    },
                    {
                    label: (
                        <Link
                        href='/contact-us'
                        style={{
                            width: "100%",
                        }}
                        >
                        <Avatar
                            style={{
                            backgroundColor: 'black',
                            }}
                        >
                            <PhoneOutlined/>
                        </Avatar>
                        <div className="text-xs"> تماس با ما </div>
                        </Link>
                    ),
                    value: 'user3',
                    },
                ]}
            />


        </div>
    );
}

export default MobileBottomBar;