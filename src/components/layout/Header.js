'use client'

import { CloseCircleFilled, CloseCircleOutlined, CloseOutlined, HomeOutlined, LoginOutlined, LogoutOutlined, MoreOutlined, ProfileOutlined, SettingOutlined, ShoppingCartOutlined, ShoppingOutlined, UserAddOutlined, UserOutlined } from "@ant-design/icons";
import { Badge, Button, Divider, Drawer, Dropdown, Input, Space } from "antd";
import Image from "next/image";
import HeadLogo from "../../../public/logo/SWP_new1.png"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { decrement, increment, removeCart } from "@/app/GlobalRedux/Features/counter/CounterSlice";
import HeaderExitBtn from "../modules/HeaderExitBtn";
import Cookies from "js-cookie";
import Link from "next/link";


const Header = () => {

  const cart = useSelector((state) => state.counter.items)
  let sumProduct = useSelector((state) => state.counter.sumProduct)
  const dispatch = useDispatch()

    const items = [
        {
          key: '1',
          label: (
            <a href="/" >
              <HomeOutlined/> خانه 
            </a>
          ),
        },
        {
          key: '2',
          label: (
            <a href="/profile" >
           <UserAddOutlined/> پروفایل 
            </a>
          ),
        },
        {
          key: '3',
          label: (
            <a href="/">
               <ShoppingOutlined/> سبد خرید 
            </a>
          ),
        },
        {
            key: '4',
            label: (
              <a href="/">
                 <SettingOutlined/>  تنظیمات 
              </a>
            ),
          },
        {
          key: '5',
          label: (
            <HeaderExitBtn/>
          ),
        },
      ];


      const [open, setOpen] = useState(false);
      const showDrawer = () => {
        setOpen(true);
      };

      const onClose = () => {
        setOpen(false);
      };

      // const CloseAndPay = () => {
      //   setOpen(false);
      //   window.location.replace("/buying-basket")
      // }

      const [coolooche, setCoolooche] = useState("");

      useEffect(() => {
        setCoolooche(Cookies.get("accessTokenSwp"))
      },[])


    return (
        <div className="w-full top-0 fixed flex flex-row justify-between p-1 items-center mb-2 border-b-2 pt-2 bg-mainGray z-50 ">
            <div>
                <Image width={70} height={70} alt="logo" src={HeadLogo} />
            </div>
              {
                coolooche
                ? 
                (
                    <div className="ml-3 flex flex-row justify-center items-center gap-3">


                      <Dropdown
                        className="max-w-[100px] max-h-[200px] "
                        menu={{
                            items,
                        }}
                        placement="bottom"
                        arrow={{
                            pointAtCenter: true,
                        }}
                        
                        >
                        <UserOutlined className="text-xl border rounded-full p-1 border-mainBlack" />
                      </Dropdown>
                        <button onClick={() => showDrawer()} >
                        <Badge count={sumProduct}>
                            <ShoppingCartOutlined className="text-xl border rounded-full p-1 border-mainBlack" />
                        </Badge>
                        </button>

                    </div>
                )
                :
                (
                  <div className="ml-3 flex flex-row justify-center items-center gap-3" >
                    <Link href="/signin" >
                      <LoginOutlined className="text-lg border rounded-full p-1 border-mainBlack text-mainBlack " />
                    </Link>
                  </div>
                )

              }

            <Drawer
                title="سبد خرید"
                placement='right'
                width={300}
                onClose={onClose}
                open={open}
                extra={
                <Space>
                    <Link href="buying-basket" className="bg-black text-white p-1 rounded-xl" >
                      <button onClick={() => onClose() } >
                         ادامه خرید
                      </button>

                    </Link>
                </Space>
                }
            >

              {
                cart.map((i) => (
                  <>
                    <div className="w-full flex flex-row h-32 border-2 rounded-xl p-1 relative bg-[#bfad9d30]  text-mainBlack font-semibold gap-2" >
                      <button onClick={() => dispatch(removeCart(i.productId))} className="absolute right- top-[-10px]"><CloseCircleFilled className="text-2xl text-red-800 font-bold"/></button>
                      <div id="sideCartBg" className="w-1/3 mt-4 h-[90%]" style={{backgroundImage:`url(${i.images[0]})`}} >
                      </div>
                      <div className="w-2/3 mt-4 flex flex-col justify-between items-end">
                        <h4 className="w-full text-sm text-right" > {i.productName} </h4>
                        <div className="flex flex-row-reverse justify-center items-center w-[80%]">
                          <button onClick={() => dispatch(decrement(i.productId)) } className="w-1/3 border-2 bg-mainBlack rounded-lg rounded-r-none h-9 text-mainGray " > - </button>
                          <Input min={2} value={i.cartQty} className="w-1/3 h-[35px] rounded-none "/>
                          <button onClick={() => dispatch(increment(i.productId)) } className="w-1/3 border-2 bg-mainBlack rounded-lg rounded-l-none h-9 text-mainGray " > + </button>
                        </div>
                      </div>
                    </div>
                    <Divider/>
                  </>
                ))
              }


            </Drawer>


        </div>
    );
}

export default Header;