'use client'

import { LoadingOutlined, SmileOutlined, SolutionOutlined, UserOutlined } from "@ant-design/icons";
import Icon from "@ant-design/icons/lib/components/Icon";
import { Avatar, Progress } from "antd";
import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";


const BuyingBasketBottomBar = ({children}) => {

    const stp = useSelector((state) => state.counter.StepNav)

    const [Steps, setSteps] = useState(Cookies.get("step"))

    useEffect(() => {
        setSteps(Cookies.get("step"))
    },[stp])

    return (
        <div className="w-full flex flex-col gap-1 justify-center items-center " >
            {children}
        </div>
    );
}

export default BuyingBasketBottomBar;