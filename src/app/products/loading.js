import { Suspense } from 'react'
import ProductsPage from '@/components/templates/ProductsPage'
import LoaderPage from '@/components/modules/LoaderPage'
 
export default function Posts() {
  return (
    <section className='w-full h-screen flex justify-center items-center text-center'>
      <Suspense fallback={<LoaderPage/>}>
        <ProductsPage/>
      </Suspense>
    </section>
  )
}