import SamplePage from "@/components/templates/SamplePage";

const Sample = ({params}) => {
    return (
        <div className="w-full mt-10">
            <SamplePage params={params} />
        </div>
    );
}

export default Sample;