import ProductsPage from "@/components/templates/ProductsPage";

const Products = () => {
    return (
        <div>
            <ProductsPage/>
        </div>
    );
}

export default Products;