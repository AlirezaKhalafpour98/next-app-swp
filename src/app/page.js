import { Carousel } from "antd";
import Image from "next/image";
import Wood from "../../public/images/Woodworker.svg"
import Truck from "../../public/images/Logistics.svg"
import Buy from "../../public/images/Online Groceries.svg"
import { WifiOutlined } from "@ant-design/icons";
import Accordion from "@/components/modules/Accordion";
import Link from "next/link";
import BackgroundMainImage from "@/components/modules/BackgroundMainImage";



export default function Home() {


  return (
    <div className="flex flex-col items-center justify-between p-1 h-full">
      <div id="HomeImg" className="h-[100vh] w-full" >
        <BackgroundMainImage/>

        <div className="w-full h-[100%]" >
          <div className="h-[100%] w-full relative bg-gradient-to-b from-transparent to-black opacity-85 " >
            <h1 className="text-white font-semibold z-10 opacity-100 absolute bottom-[30%] text-2xl bg-black rounded-lg"> کالای چوب سلطانی </h1>
            <h3 className="text-white font-semibold z-10 opacity-100 absolute bottom-[23%] text-base bg-black rounded-lg" > فروش انواع ام دی اف و پاک چوب </h3>
          </div>
        </div>
      </div>


      <div className="w-full h-[60vh] flex flex-row items-center" >

        <Image src={Buy} priority width={230} height={230} />

        <div className="text-center gap-2 flex flex-col" >

          <p> آنلاین خریدت رو انجام بده <WifiOutlined/>  </p>
          <Link href="/products" className="p-2 rounded-2xl bg-mainBlack text-white"> فروشگاه </Link>

        </div>
        
      </div>


      <div className="h-[92vh] w-full">
        <Carousel pauseOnHover dotPosition='left' draggable autoplay infinite="false" className="h-full w-full">
          <div className=" h-[90vh] bg-gray-400 flex flex-col w-full text-center " >
            <Image src={Wood} width={350} height={350} />
            <h4 className="text-2xl" > کابینت سازی </h4>
          </div>
          <div className=" h-[90vh] bg-gray-400 flex flex-col w-full text-center " >
          <Image  src={Truck} width={350} height={350} />
            <h4 className="text-2xl" > کابینت سازی </h4>
          </div>
        </Carousel>
      </div>

      <div className="h-[92vh] w-full">
        <Accordion/>
      </div>

    </div>
  )
}
