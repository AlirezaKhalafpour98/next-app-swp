import SigninPage from "@/components/templates/SigninPage";

const SignIn = () => {
    return (
        <div>
            <SigninPage/>
        </div>
    );
}

export default SignIn;