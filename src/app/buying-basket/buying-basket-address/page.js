'use client'

import { Stepper } from "@/app/GlobalRedux/Features/counter/CounterSlice";
import CustomNeshanMap from "@/components/modules/CustomNeshanMap";
import { e2p } from "@/utils/replaceNumber";
import { EnvironmentFilled, FileTextOutlined, HomeOutlined, MobileOutlined, ShopFilled, UserOutlined } from "@ant-design/icons";
import { Avatar, Button, Drawer, Input, Progress } from "antd";
import TextArea from "antd/es/input/TextArea";
import Cookies from "js-cookie";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";


const BuyingBasketAddress = () => {

    const dispatch = useDispatch()

    const [drawer, setDrawer] = useState(false)
    const [address, setAddress] = useState("")

    const NextStep = () => {
        Cookies.set('step', 60)
        dispatch(Stepper(3))
    }


    return (
        <>

            <div className="w-full flex flex-col gap-1 justify-center items-center mt-8 overflow-hidden " >

                <div className="w-full flex flex-row justify-normal items-center" >
                    <div className="w-1/5 text-center " >
                        <Avatar className="bg-yellow-500" size={"large"} icon={<EnvironmentFilled />} />
                    </div>
                    <div className="w-4/5 flex flex-col items-start  " >
                        <p className="text-sm" > مرحله {e2p(2)} از {e2p(5)} </p>
                        <p className="text-lg text-yellow-600 font-semibold " > آدرس تحویل </p>
                    </div>
                </div>

                <div className="w-[90%]" >

                    <Progress percent={40} status="active" showInfo={false} />

                </div>
            </div>

            <div  className="w-full h-[80vh] flex flex-col gap-4 justify-center items-center " >

                <button className="border bg-mainBlack text-white p-2 rounded-lg" onClick={() => setDrawer(true)}>
                    مشاهده آدرس <EnvironmentFilled/>
                </button>

                <div>
                    <CustomNeshanMap setAddress={setAddress} />
                </div>

            <Drawer
                title=" جزییات آدرس "
                placement="bottom"
                closable={false}
                onClose={() => setDrawer(false)}
                open={drawer}
                height={500}
            >
                <div className=" bg-mainGray border-2 border-b-0 rounded-t-3xl flex flex-col gap-7 justify-start pt-6 items-center text-center w-[95%] mx-auto h-[80%] p-2 " >

                    <span className="w-16 h-2 rounded-2xl bg-yellow-500 " ></span>

                    <Input addonBefore={<HomeOutlined />} className="border border-mainBlack rounded-lg" placeholder=" نام مکان تحویل گیرنده (کارگاه، خانه و ...)" />
                    <Input addonBefore={<FileTextOutlined />} className="border border-mainBlack rounded-lg" placeholder=" نام فرد تحویل گیرنده " />
                    <Input addonBefore={<MobileOutlined />} className="border border-mainBlack rounded-lg" placeholder=" شماره تماس تحویل گیرنده " />
                    <TextArea
                        className="border border-mainBlack rounded-lg"
                        placeholder=" آدرس دقیق "
                        value={address}
                        autoSize={{
                        minRows: 3,
                        maxRows: 5,
                        }}
                    />

                    <div className="w-full justify-around flex flex-row items-center gap-1" >
                        <Link href="/buying-basket/buying-basket-waiting" className="bg-yellow-500 p-2 w-28 rounded-xl text-mainBlack" onClick={() => NextStep()} > ثبت آدرس </Link>
                        <Link href="/buying-basket/buying-basket-waiting" className="bg-yellow-500 p-2 w-auto flex flex-row gap-1 justify-center items-center rounded-xl text-mainBlack" onClick={() => NextStep()} > <ShopFilled />   تحویل در محل </Link>
                    </div>


                </div>
            </Drawer>


                
            </div>
        </>
    );
}

export default BuyingBasketAddress;