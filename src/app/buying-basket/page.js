import BuyingBasketPage from "@/components/templates/BuyingBasketPage";

const page = () => {
    return (
        <div>
            <BuyingBasketPage/>
        </div>
    );
}

export default page;