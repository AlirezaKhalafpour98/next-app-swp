'use client'

import { e2p } from "@/utils/replaceNumber";
import { Avatar, Divider, Progress, Statistic } from "antd";
import waitGif from "../../../../public/images/Waiting.gif"
import Image from "next/image";
import { useState } from "react";
import Link from "next/link";
import Cookies from "js-cookie";
import { useDispatch } from "react-redux";
import { Stepper } from "@/app/GlobalRedux/Features/counter/CounterSlice";
import { CustomerServiceFilled } from "@ant-design/icons";



const BuyingBasketWaiting = () => {

    const [modal , setModal] = useState(false)

    const {Countdown} = Statistic
    const deadline = Date.now() + 1000  * 60 * 45  + 1000 * 30;

    const dispatch = useDispatch()

    const NextStep = () => {
        Cookies.set('step', 80)
        dispatch(Stepper(4))
    }

    return (

        <>

            <div className="w-full flex flex-col gap-1 justify-center items-center mt-8 " >

            <div className="w-full flex flex-row justify-normal items-center" >
                <div className="w-1/5 text-center " >
                    <Avatar className="bg-yellow-500" size={"large"} icon={<CustomerServiceFilled />} />
                </div>
                <div className="w-4/5 flex flex-col items-start  " >
                    <p className="text-sm" > مرحله {e2p(3)} از {e2p(5)} </p>
                    <p className="text-lg text-yellow-600 font-semibold " > بررسی سبد خرید  </p>
                </div>
            </div>

            <div className="w-[90%]" >

                <Progress percent={60} status="active" showInfo={false} />

            </div>
            </div>

            <div className="w-full h-[85vh] flex flex-col justify-center items-center text-center " >

                <Image src={waitGif} width={300} height={300} />

                <Countdown title=" مدت زمان انتظار " value={deadline} format="HH:mm:ss:SSS" />

                <Divider/>

                <h3>
                لطفا جهت بررسی و دریافت قیمت از سوی کارشناسان ما صبورانه منتظر باشید...
                شماره تماس واحد پشتیبانی: 07138310032               
                </h3>

                <Link href='/buying-basket/buying-basket-payment' onClick={() => NextStep()} > نهایی </Link>
            </div>


        </>
    );
}

export default BuyingBasketWaiting;