'use client'

import Image from "next/image";
import paycard from "../../../../public/images/paycard.gif"
import { addCommas, e2p } from "@/utils/replaceNumber";
import { Avatar, Button, Progress } from "antd";
import { CreditCardFilled } from "@ant-design/icons";
import { useSelector } from "react-redux";
import { numberToWords } from "@persian-tools/persian-tools";

const BasketPayment = () => {

    const WholePrice = useSelector((state) => state.counter.sumPrice)

    return (

        <>

            <div className="w-full flex flex-col gap-1 justify-center items-center " >

            <div className="w-full flex flex-row justify-normal items-center" >
                <div className="w-1/5 text-center " >
                    <Avatar className="bg-yellow-500" size={"large"} icon={<CreditCardFilled />} />
                </div>
                <div className="w-4/5 flex flex-col items-start  " >
                    <p className="text-sm" > مرحله {e2p(4)} از {e2p(5)} </p>
                    <p className="text-lg text-yellow-600 font-semibold " >  پرداخت </p>
                </div>
            </div>

            <div className="w-[90%]" >

                <Progress percent={80} status="active" showInfo={false} />

            </div>
            </div>

            <div className="flex flex-col justify-center items-center h-[70vh] w-full text-center" >
                <div className=" flex flex-col justify-center items-center text-center gap-4 p-4 rounded-2xl shadow-md shadow-black  border-2 w-[70%]" >
                    <Image
                        className="rounded-2xl"
                        src={paycard}
                        width={250}
                        height={250}
                    />
                    <h3> مبلغ قابل پرداخت </h3>

                    <h3> {e2p(addCommas(WholePrice))} </h3>
                    <h3> {numberToWords(WholePrice)} </h3>

                    <Button className="w-[80%] bg-mainBlack text-white " > پرداخت </Button>
                </div>
            </div>

        </>
    );
}

export default BasketPayment;