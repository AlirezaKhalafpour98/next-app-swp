import BuyingBasketBottomBar from "@/components/layout/BuyingBasketBottomBar";

const layout = ({children}) => {
    return (
        <>
        <BuyingBasketBottomBar>
            {children}
        </BuyingBasketBottomBar>
        </>
    );
}

export default layout;