import Image from "next/image";
import contactus from "../../../public/images/Contactus.gif"
import branch1 from "../../../public/images/Basij.jpg"
import branch2 from "../../../public/images/IMG-20230102-WA0019.jpg"
import branch3 from "../../../public/images/IMG_20221225_164327.jpg"


const ContactUs = () => {
    return (
        <div className="w-full flex flex-col text-center gap-8">
            <div className="w-full flex justify-center items-center text-center" >
                <Image className="bg-transparent rounded-full" src={contactus} width={300} height={300} />
            </div>
            <div>
                <div className="w-full text-center flex justify-center mb-8">
                    <h2 className="border-b-4 px-3 py-1 border-yellow-600 w-fit font-semibold"> شعبه های ما </h2>
                </div>

                <div className="flex flex-col justify-around items-center w-full gap-8" >
                    <Image width={200} height={200} src={branch1} className="rounded-full"/>

                        <h3 className="font-semibold" >آدرس:</h3> 
                        <p className="w-[80%]" >بلوار ابوذر غفاری،نرسیده به محراب خیابان کار کوچه 3 سوله روبرو</p>
                        <h3 className="font-semibold" >شماره های تماس:</h3>
                    <div className="grid grid-cols-3 gap-3 pb-2 border-b-2 " >

                        <p> 071-37432542 </p>
                        <p> 09177017801 </p>
                        <p> 09170519281 </p>
                        <p> 09170519284 </p>
                        <p> 09170519237 </p>
                        <p> 09171152747 </p>
                        <p> 071-37432654 </p>
                        <p> 071-37432542 </p>

                    </div>

                    <Image width={200} height={200} src={branch2} className="rounded-full"/>

                    
                    <h3 className="font-semibold" >آدرس:</h3> 
                        <p className="w-[80%]" >  ایران، شیراز، شهرک صنعتی، میدان اول، بلوار کارگر جنوبی 3 </p>
                        <h3 className="font-semibold" >شماره های تماس:</h3>
                        <p> تلفکس :07137743587   </p>

                    <div className="grid grid-cols-3 gap-3 pb-2 border-b-2 " >

                        <p> 071-37742578 </p>
                        <p> 071-37742067 </p>
                        <p> 071-37740073 </p>
                        <p> 071-37743587 </p>
                        <p> 071-37744452 </p>
                    </div>


                    <Image width={200} height={200} src={branch3} className="rounded-full"/>
                    <h3 className="font-semibold" >آدرس:</h3> 
                        <p className="w-[80%]" >بلوار ابوذر غفاری،نرسیده به محراب خیابان کار کوچه 3 سوله روبرو</p>
                        <h3 className="font-semibold" >شماره های تماس:</h3>
                        <p> تلفکس : 07137743587  </p>
                    <div className="grid grid-cols-2 gap-3 pb-2 " >

                        <p> 071-36476588 </p>
                        <p> 09170519237 </p>

                    </div>
                </div>

            </div>
        </div>
    );
}

export default ContactUs;