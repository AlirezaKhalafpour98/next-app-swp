

import './globals.css'

import { Providers } from './GlobalRedux/provider'
import dynamic from 'next/dynamic'

export const metadata = {
  title: ' کالای چوب سلطانی ',
  description: ' فروشگاه کالای چوب نمایندگی AGT ',
}

const Header = dynamic(() => import("@/components/layout/Header"))
const MobileBottomBar = dynamic(() => import("@/components/layout/MobileBottomBar"),{
  loading: () => <p> i am loading ... </p>
})



export default function RootLayout({ children }) {

    return (
      <html lang="fa">
        <body className='mb-24 mt-4 !bg-mainGray'>
          <Providers>
            <Header className=" fixed top-0"/>
            {children}
            <MobileBottomBar className="bottom-0"/>
          </Providers>
        </body>
      </html>
    )

}
