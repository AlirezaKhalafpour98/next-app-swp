import SignUpPage from "@/components/templates/SignUpPage";

const Signup = () => {
    return (
        <div>
            <SignUpPage/>
        </div>
    );
}

export default Signup;