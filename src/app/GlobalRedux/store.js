'use client'

import {configureStore} from "@reduxjs/toolkit"
import CounterSlice from "./Features/counter/CounterSlice"
import FilterProductsDataSlice from "./Features/filterData/FilterProductsDataSlice"

export const store = configureStore({
    reducer:{
        counter: CounterSlice,
        filteredData: FilterProductsDataSlice,
    }
})