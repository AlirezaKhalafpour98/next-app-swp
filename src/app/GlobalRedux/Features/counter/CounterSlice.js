'use client'

import { createSlice } from "@reduxjs/toolkit"
import Cookies from "js-cookie"


  const initialState={
     items: [],
     sumProduct: 0,
     sumPrice: 0,
     StepNav:0,
    }


export const counterSlice = createSlice({
    
    name: 'counter',
    initialState,
    reducers:{
        increment : (state, action) => {
            const itemIndex = state.items.findIndex(
                (item) => item.productId === action.payload
            )
            state.items[itemIndex].cartQty += 1
            state.sumPrice += state.items[itemIndex].salePrice
            state.sumProduct += 1
            
        },
        decrement : (state, action) => {
            const itemIndex = state.items.findIndex(
                (item) => item.productId === action.payload
            )
            if(state.items[itemIndex].cartQty <= 1){
                const nextCartItem = state.items.filter(
                    (cartItem) => cartItem.productId !== action.payload
                )
    
                state.items = nextCartItem
                state.sumProduct -= 1
                state.sumPrice -= state.items[itemIndex].salePrice
                
            } else {
                state.items[itemIndex].cartQty -= 1
                state.sumProduct -= 1
                state.sumPrice -= state.items[itemIndex].salePrice
                
            }
        },
        addCart: (state, action) =>{
            // const itemIndex = state.items.findIndex(
            //     (item) => item.productId === action.payload.productId
            // )
            // if(itemIndex >= 0){
            //     state.items[itemIndex].cartQty += 1
            //     state.sumProduct += 1
            // }else {
            //     const tempProduct = {...action.payload, cartQty: 1}
            //     state.items.push(tempProduct)
            //     state.sumProduct += 1
            // }

            const itemIndex = state.items.findIndex(
                (item) => item.productId === action.payload.productId
            )

            if(itemIndex >= 0){
                state.reRender += 5
            }else {
                const tempProduct = {...action.payload, cartQty: 1, IsInListMessage:true}
                state.items.push(tempProduct)
                state.sumProduct += 1
                state.sumPrice += action.payload.salePrice
            }
        },
        removeCart: (state, action) =>{
            const nextCartItem = state.items.filter(
                (cartItem) => cartItem.productId !== action.payload
            )
            const itemIndex = state.items.findIndex(
                (item) => item.productId === action.payload
            )

            state.items = nextCartItem
            if(state.items.length < 1){
                state.sumProduct = 0
                state.sumPrice = 0
                
            }else {
                state.sumProduct -= state.items[itemIndex].cartQty
                state.sumPrice -= state.items[itemIndex].salePrice * state.items[itemIndex].cartQty
            }
        },

        Stepper : (state, action) => {
            state.StepNav = action.payload

        },
    }

})

export const {increment, decrement, addCart, removeCart, Stepper} = counterSlice.actions;

export default counterSlice.reducer