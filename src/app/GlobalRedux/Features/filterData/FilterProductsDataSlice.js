'use client'

import { createSlice } from "@reduxjs/toolkit"


  const initialState={
     productItems: [],
    }


export const FilterProductsDataSlice = createSlice({
    
    name: 'filteredData',
    initialState,
    reducers:{
        FilteredProductList : (state, action) => {
            state.productItems = action.payload
        },
    }

})

export const {FilteredProductList} = FilterProductsDataSlice.actions;

export default FilterProductsDataSlice.reducer